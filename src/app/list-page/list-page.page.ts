import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

//Models
import { Developer } from "../models/developer";
import { TeamLeader } from "../models/teamLeader";

//Global Data
import { GlobalData } from '../globalData/globalData';

@Component({
  selector: 'app-list-page',
  templateUrl: './list-page.page.html',
  styleUrls: ['./list-page.page.scss'],
})
export class ListPagePage implements OnInit {

  users: [TeamLeader | Developer] = [] as any
  loading: HTMLIonLoadingElement

  constructor(private apiServices: ApiService,
    public loadingCtrl: LoadingController,
    private router: Router,
    private globalData: GlobalData ) { }

  ngOnInit() {
    this.presentLoading()
    this.getTeams()
  }

  //Init Loading Spinner
  private async presentLoading() {
    this.loading = await this.loadingCtrl.create({
      message: 'Loading users...'
    });
    return await this.loading.present();
  }

  //Get all users from the request
  private getTeams(){
    this.apiServices.getUsers().subscribe(data => {
      data.forEach(element => {
        let user: TeamLeader | Developer;
        if(element.type === "team-leader")  user = new TeamLeader(element)
        else                                user = new Developer(element)
        this.users.push(user);
      });
      this.loading.dismiss()
      this.globalData.setUsers = this.users
    }, error => {
      console.log(error);
    })
  }

  //Navigation to User Detail Page
  userDetail(user: TeamLeader | Developer){
    this.router.navigate(['/details/'+user.getId]);
  }

}
    