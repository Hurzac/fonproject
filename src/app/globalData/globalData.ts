import TeamLeader from "../models/teamLeader";
import Developer from "../models/developer";
import User from "../models/user";

export class GlobalData{
    private users: [TeamLeader | Developer]

    constructor(){

    }

    
    public get getUsers() : [TeamLeader | Developer] {
        return this.users
    }
    
    
    public set setUsers(users : [TeamLeader | Developer]) {
        this.users = users;
    }

    
    public getUserById(id: string) : TeamLeader | Developer {
        let user: TeamLeader | Developer
        this.getUsers.forEach(element => {
            if (element.getId === id)   user = element
        });
        return user
    }

    public updateUserData(user: User){
        this.getUsers.forEach(element => {
            if(element.getId === user.getId)    element = user;
        });
    }
}