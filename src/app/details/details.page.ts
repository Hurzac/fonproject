import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GlobalData } from '../globalData/globalData';
import { DomSanitizer } from '@angular/platform-browser';

//Models
import Developer from '../models/developer';
import TeamLeader from '../models/teamLeader';

//Native imports
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ToastController } from '@ionic/angular';

//Services
import { ApiService } from '../api.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  id: string
  currentUser: TeamLeader | Developer
  sanitizedImg: any;

  options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  };

  constructor( private route: ActivatedRoute, 
    private globalData: GlobalData,
    private camera: Camera,
    private sanitizer: DomSanitizer,
    private webView: WebView,
    private apiServices: ApiService,
    private toastController: ToastController) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    console.log(this.id);
    this.currentUser = this.globalData.getUserById(this.id);
    this.sanitizedImg = this.sanitizer.bypassSecurityTrustUrl(this.currentUser.getAvatar)
  }

  //Init Toast
  private async presentToast() {
    let toast = await this.toastController.create({
      message: 'Your data have been saved.',
      duration: 2000,
      position: 'bottom'
    });
    return await toast.present();
  }

  //Addapt the URL of the image to show it in the HTML
  private getSanitizeURL(){
    this.sanitizedImg = this.webView.convertFileSrc(this.currentUser.getAvatar);
  }
  
  //Take a picture with camera and save it
  openCamera(){
    this.camera.getPicture(this.options).then((imageData) => {
      this.currentUser.setAvatar = imageData;
      this.getSanitizeURL();
     }, (err) => {
       console.log(err);
     });
  }

  //Save modified data 
  saveData(){
    this.presentToast();
    this.globalData.updateUserData(this.currentUser);
    this.apiServices.updateUser(this.currentUser).subscribe(data =>{
      console.log(data);
      this.presentToast();
    }, error => {
      console.log(error);
    });
  }
}
