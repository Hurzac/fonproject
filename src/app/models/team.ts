class Team {
    private id: string
    private name: string

    constructor(team?: Team) {
        this.id = team.getId
        this.name = team.getName
    }

    public get getId() : string {
        return this.id
    }
    
    public set setId(id: string){
        this.id = id
    }

    public get getName() : string {
        return this.name
    }

    public set setName(name: string) {
        this.name = name
    }
     
}