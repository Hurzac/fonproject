import { User } from "../models/user";

export class TeamLeader extends User {
    private teamLeaderOf: string

    constructor(user?: TeamLeader){
        super(user)
        this.teamLeaderOf = user.teamLeaderOf
    }
    
    public get getTeamLeaderOf(): string{
        return this.teamLeaderOf
    }
    public set setTeamLeaderOf(teamLeaderOf: string){
        this.teamLeaderOf = teamLeaderOf
    }
}
export default TeamLeader