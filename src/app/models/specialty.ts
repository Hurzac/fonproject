class Speciality{
    private id: string
    private name: string

    constructor(speciality?: Speciality) {
        this.id = speciality.getId
        this.name = speciality.getName
    }

    public get getId() : string {
        return this.id
    }
    
    public set setId(id: string){
        this.id = id
    }

    public get getName() : string {
        return this.name
    }

    public set setName(name: string) {
        this.name = name
    }
}