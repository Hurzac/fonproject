export class User {
    private id: string
    private name: string
    private surname: string
    private age: number
    private email: string
    private avatar: string
    private type: string
    private speciality: Speciality

    constructor(user?: User) {
        this.id = user.id
        this.name = user.name
        this.surname = user.surname
        this.age = user.age
        this.email = user.email
        this.avatar = user.avatar
        this.type = user.type
    }

    public get getId() : string {
        return this.id
    }
    
    public set setId(id: string){
        this.id = id
    }

    public get getName() : string {
        return this.name
    }

    public set setName(name: string) {
        this.name = name
    }

    public get getSurname() : string {
        return this.surname
    }
    
    public set setSurname(surname: string){
        this.surname = surname
    }

    public get getAge() : number {
        return this.age
    }

    public set setAge(age: number) {
        this.age = age
    }

    public get getEmail() : string {
        return this.email
    }
    
    public set setEmail(email: string){
        this.email = email
    }

    public get getAvatar() : string {
        return this.avatar
    }

    public set setAvatar(avatar: string) {
        this.avatar = avatar
    }

    public get getType() : string {
        return this.type
    }

    public set setType(type: string) {
        this.type = type
    }

    public get getSpeciality() : Speciality {
        return this.speciality
    }

    public set setSpeciality(speciality: Speciality) {
        this.speciality = speciality
    }
    
}

export default User