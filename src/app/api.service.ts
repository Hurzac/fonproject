import { Injectable } from '@angular/core';
import { HttpParams} from  '@angular/common/http';
import { ConfigProvider } from './services-config/api.config';
import { Observable } from 'rxjs';

//Network config
import { NetworkProvider } from "./services-config/network.service";
import TeamLeader from './models/teamLeader';
import Developer from './models/developer';
import User from './models/user';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private baseUrl: string;
  private services: any;
  private servicesMock: any;

  constructor(private network: NetworkProvider, private config: ConfigProvider) { 
    this.baseUrl = this.config.get('baseUrl');
    this.services = this.config.get('services');
    this.servicesMock = this.config.get('servicesMock');
  }

  //Request to get all users
  getUsers(): Observable <any>{
    let httpParams = new HttpParams()
    const path = this.servicesMock.users;
    return  this.network.get(`${this.baseUrl}${path}`, httpParams);
  }

  //Request to update de User Data
  updateUser(userData: User): Observable <any>{
    let httpParams = new HttpParams()
    .append("name", userData.getName)
    .append("surname", userData.getSurname)
    .append("age", userData.getAge.toString())
    .append("email", userData.getEmail)
    .append("avatar", 'data:image/jpeg;base64,'+userData.getAvatar)
    const path = this.servicesMock.updateUser;
    return  this.network.post(`${this.baseUrl}${path}`, httpParams);
  }

}
