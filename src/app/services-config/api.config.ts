import { Injectable } from '@angular/core';
import { parameters } from './api.config.parameters';


@Injectable({
  providedIn: 'root'
})
export class ConfigProvider {

  
  private config: any;

  constructor() {
    this.config = parameters.envs['dev'];
  }

  get(key: string): any {
    return this.config[key] || parameters.defaults[key];
  }

}