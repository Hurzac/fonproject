import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';


@Injectable({
    providedIn: 'root'
  })
export class NetworkProvider {

  constructor(private http: HttpClient) {
   }

  get(url: string, params: HttpParams): Observable<any> {
    return Observable.create((observer: Observer<any>) => {
      this.http.get(url, { headers: this.getAuthHeaders(), params: params }).subscribe((data: any) => {
          observer.next(data); 
      }, err => {
        observer.error(err);
        observer.complete();
      });
    });
  }

  post(url: string, params: HttpParams): Observable <any> {
    return Observable.create((observer: Observer<any>) => {
      this.http.post(url, { headers: this.getAuthHeaders(), params: params }).subscribe((data: any) => {
          observer.next(data);
      }, err => {
        observer.error(err);
        observer.complete();
      });
    }, error =>{
      console.log(error);
    });
  }

  getAuthHeadersPost(p_auth?: string): HttpHeaders {
    let headers: any = {};
    headers = {
        'cache-control': 'no-cache, private, no-store, must-revalidate',
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
    }
    return new HttpHeaders(headers);
  }


  getAuthHeaders(p_auth?: string): HttpHeaders {
    let headers: any = {};
    headers = {
        'Content-Type': 'application/json; charset=utf-8',
        'cache-control': 'no-cache, private, no-store, must-revalidate'
    }
    return new HttpHeaders(headers);
  }

}
