export const parameters = {
    envs: {
        dev: {
            baseUrl: 'http://demo0194535.mockable.io',
            // baseUrl: 'http://localhost:8080'
        }
    },
    defaults: {
        services: {
            teams: '/teams',
            specialities: '/specialties',
            users: '/users/{id}',
            teamOf: '/users/{userId}/developers/{userId}',
            userSpecislities: '/users/{userId}/specialties/{specialtyId}'
        },
        servicesMock:{
            users: '/users', //GET
            updateUser: '/usersUpdate', // POST
        }
    }
};